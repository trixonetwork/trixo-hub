package com.trixo.hub.player;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.plugin.Plugin;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.data.database.MongoDB;
import com.trixo.hub.TrixoHub;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetManager;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.permissions.nodes.TrixoPermissions;
import com.trixo.server.player.TrixoPlayer;
import com.trixo.utils.EntityUtils;

@SuppressWarnings("unchecked")
public class HubPlayer extends TrixoPlayer {
    /** Constructor **/
    public HubPlayer(Plugin plugin, Player thePlayer) {
        super(plugin, thePlayer);

        this.getData().put("gadget.taskIDs", new HashMap<GadgetType, Integer>());
        this.getData().put("gadget.activeGadgets", new HashMap<GadgetType, Gadget>());
    }

    /** HubPlayer Functions **/
    /* Gadget Functions */
    public void activateGadget(final Gadget gadget) {
        // if (this.hasGadget(gadget)) {
        final HubPlayer player = this;

        if (this.getActiveGadget(gadget.getType()) != null) {
            this.deactivateGadget(gadget.getType());
        }

        gadget.onActivated(this);
        gadget.register(DataCenter.retrieveData("hub", "plugin", TrixoHub.class));

        if (gadget.doTick()) {
            HashMap<GadgetType, Integer> taskIDs = (HashMap<GadgetType, Integer>) this.getData().get("gadget.taskIDs");

            taskIDs.put(gadget.getType(), Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(
                    DataCenter.retrieveData("hub", "plugin", TrixoHub.class), new Runnable() {
                        @Override
                        public void run() {
                            gadget.onTick(player);
                        }
                    }, 0L, gadget.getTickSpeed()));

            this.getData().put("gadget.taskIDs", taskIDs);
        }

        this.getActiveGadgets().put(gadget.getType(), gadget);
        // }
    }

    public void deactivateGadget(GadgetType type) {
        if (this.getActiveGadget(type) != null) {
            {
                HashMap<GadgetType, Integer> taskIDs = (HashMap<GadgetType, Integer>) this.getData()
                        .get("gadget.taskIDs");

                if (taskIDs.containsKey(type)) {
                    Bukkit.getServer().getScheduler().cancelTask(taskIDs.get(type));
                    taskIDs.remove(type);
                }

                this.getData().put("gadget.taskIDs", taskIDs);
            }

            this.getActiveGadget(type).unregister();
            this.getActiveGadget(type).onDeactivated(this);
        }

        this.getActiveGadgets().put(type, null);
    }

    public void deactivateGadgets() {
        for (Gadget gadget : this.getActiveGadgets().values()) {
            {
                HashMap<GadgetType, Integer> taskIDs = (HashMap<GadgetType, Integer>) this.getData()
                        .get("gadget.taskIDs");

                if (taskIDs.containsKey(gadget.getType())) {
                    Bukkit.getServer().getScheduler().cancelTask(taskIDs.get(gadget.getType()));
                    taskIDs.remove(gadget.getType());
                }

                this.getData().put("gadget.taskIDs", taskIDs);
            }

            gadget.unregister();
            gadget.onDeactivated(this);
        }

        this.getActiveGadgets().clear();
    }

    public void giveGadget(Gadget gadget) {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        {
            BasicDBObject toInsert = new BasicDBObject();
            toInsert.put("player", this.getUUID().toString());
            toInsert.put("gadgetID", gadget.getID());

            DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
            iData.put("collection", "gadgets");
            iData.put("query", toInsert);
            iData.put("data", toInsert);
            iData.put("multi", false);

            database.upsert(iData);
        }

        database.disconnect();
    }

    public void takeGadget(Gadget gadget) {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        {
            BasicDBObject query = new BasicDBObject();
            query.put("player", this.getUUID().toString());
            query.put("gadgetID", gadget.getID());

            DataContainer<String, Object> rData = new DataContainer<String, Object>(false);
            rData.put("collection", "gadgets");
            rData.put("query", query);

            database.remove(rData);
        }

        database.disconnect();
    }

    public boolean hasGadget(Gadget gadget) {
        return this.getOwnedGadgets().contains(gadget) ? true : false;
    }

    /** Getters **/
    /* Getters */
    public HashMap<GadgetType, Gadget> getActiveGadgets() {
        return (HashMap<GadgetType, Gadget>) this.getData().get("gadget.activeGadgets");
    }

    public Gadget getActiveGadget(GadgetType type) {
        return this.getActiveGadgets().get(type);
    }

    public ArrayList<Gadget> getOwnedGadgets() {
        GadgetManager gadgetManager = DataCenter.retrieveData("hub", "gadgetManager", GadgetManager.class);

        {
            ArrayList<Gadget> gadgetList = new ArrayList<Gadget>();

            DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
            cData.put("host", "localhost");
            cData.put("port", 27017);
            cData.put("database", "trixo");
            cData.put("password", "");

            MongoDB database = new MongoDB();
            database.connect(cData);

            {
                BasicDBObject query = new BasicDBObject();
                query.put("player", this.getUUID().toString());

                DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
                sData.put("selectAll", false);
                sData.put("collection", "gadgets");
                sData.put("query", query);

                DataContainer<String, Object> results = database.select(sData);

                DBCursor result = (DBCursor) results.get("result");
                while (result.hasNext()) {
                    DBObject obj = result.next();

                    gadgetList.add(gadgetManager.getGadget((String) obj.get("gadgetID")));
                }
            }

            database.disconnect();

            return gadgetList;
        }
    }

    /** Events **/
    /* Bukkit Events */
    @EventHandler
    public void onToggleFlight(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();

        {
            if (player.getGameMode() == GameMode.CREATIVE) {
                return;
            }

            if (this.isMorphed() && this.getMorph().canFly()) {
                return;
            }
        }

        player.playSound(player.getLocation(), Sound.GHAST_FIREBALL, 1.F, 1.F);
        EntityUtils.setVelocity(player, 1.4, 0.2, 1, true);

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntityType() != EntityType.PLAYER) {
            return;
        }

        if (event.getEntity() == this.getPlayerInstance()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.getEntityType() != EntityType.PLAYER) {
            return;
        }

        if (event.getEntity() == this.getPlayerInstance()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {        
        if (!this.hasPermission(TrixoPermissions.BUILD.getPermissionNode())) {
            event.setCancelled(true);
        } else {
            if (this.getPlayerInstance().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!this.hasPermission(TrixoPermissions.BUILD.getPermissionNode())) {
            event.setCancelled(true);
        } else {
            if (this.getPlayerInstance().getGameMode() != GameMode.CREATIVE) {
                event.setCancelled(true);
            }
        }
    }

    /* Custom Events */
    @Override
    public void onJoin() {

    }

    @Override
    public void onLeave() {
        for (GadgetType type : GadgetType.values()) {
            this.deactivateGadget(type);
        }
    }
}

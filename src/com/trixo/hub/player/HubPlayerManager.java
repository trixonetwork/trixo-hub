package com.trixo.hub.player;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import com.trixo.server.permissions.GroupManager;
import com.trixo.server.permissions.PermissionGroup;
import com.trixo.server.player.PlayerManager;

public class HubPlayerManager extends PlayerManager {
    /** Constructor **/
    public HubPlayerManager(Plugin plugin) {
        super(plugin);
    }

    /** Events **/
    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        this.addPlayer(event.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        this.removePlayer(event.getPlayer());
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        e.setFormat("%s %s");
    }

    /** Getters, Adders **/
    /* Getters */
    @Override
    public HubPlayer getPlayer(Player player) {
        return (HubPlayer) this.getPlayers().get(player.getUniqueId());
    }

    @Override
    public HubPlayer getPlayer(UUID uuid) {
        return (HubPlayer) this.getPlayers().get(uuid);
    }

    /* Adders */
    @Override
    public void addPlayer(Player player) {
        HubPlayer tmp = new HubPlayer(this.getPlugin(), player);
        tmp.registerPlayer();

        {
            PermissionGroup group = tmp.getPermissionGroup();

            if (group != null && group != GroupManager.getPermissionGroup("DEFAULT")) {
                tmp.setPermissionGroup(group);
            } else {
                tmp.setPermissionGroup(GroupManager.getPermissionGroup("DEFAULT"));
            }
        }

        this.addPlayer(tmp);
    }
    
    /** PlayerManager Functions **/
    public void loadPlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.addPlayer(player);
        }
    }
}
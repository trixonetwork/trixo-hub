package com.trixo.hub.player;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.hub.menus.PlayerMenu;
import com.trixo.server.event.ITrixoEvent;

public class PlayerEvents implements ITrixoEvent {
    /** Events **/
    /* Register */
    @Override
    public void register(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        HandlerList.unregisterAll(this);
    }

    /* Events */
    @EventHandler
    private void onPlayerInteract(PlayerInteractEntityEvent event) {
        if (event.getRightClicked() instanceof CraftPlayer) {
            HubPlayerManager hubPlayerManager = DataCenter.retrieveData("hub", "hubPlayerManager", HubPlayerManager.class);
            HubPlayer targetPlayer = hubPlayerManager.getPlayer((Player) event.getRightClicked());
            HubPlayer player = hubPlayerManager.getPlayer(event.getPlayer());

            if (player != null) {
                DataContainer<String, Object> data = new DataContainer<String, Object>(false);
                data.put("player", player);
                data.put("targetPlayer", targetPlayer);

                PlayerMenu menu = new PlayerMenu(54, targetPlayer.getName(), data);
                menu.create();

                player.openMenu(menu);
            }
        }
    }
}

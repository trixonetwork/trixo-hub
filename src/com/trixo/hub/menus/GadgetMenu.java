package com.trixo.hub.menus;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetManager;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.player.HubPlayer;
import com.trixo.server.inventory.Menu;
import com.trixo.server.inventory.MenuButton;
import com.trixo.server.inventory.MenuButtonEvent;
import com.trixo.server.inventory.MenuPage;
import com.trixo.utils.ItemUtils;

public class GadgetMenu extends Menu {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public GadgetMenu(int size, String name, DataContainer<String, Object> data) {
        super();

        this.data = new DataContainer<String, Object>(false);
        this.data.put("player", data.get("player"));
        this.putPage("landing", new MenuPage(this, size, name, this.getPlayer()));
    }

    /** GadgetMenu Functions **/
    public void createCategories() {
        // Particle Effects
        {
            ItemStack stack = new ItemStack(Material.NETHER_STAR, 1);

            {
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName("Particle Effects");
                stack.setItemMeta(meta);

                ItemUtils.addGlow(stack);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player player, ItemStack itemStack) {
                        showPage("landing.particles", player);
                    }
                };

                MenuButton button = new MenuButton(10, stack, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }

        // Pets
        {
            ItemStack stack = new ItemStack(Material.LEASH, 1);

            {
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName("Pets");
                stack.setItemMeta(meta);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player player, ItemStack itemStack) {
                        showPage("landing.pets", player);
                    }
                };

                MenuButton button = new MenuButton(12, stack, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }

        // Mounts
        {
            ItemStack stack = new ItemStack(Material.SADDLE, 1);

            {
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName("Mounts");
                stack.setItemMeta(meta);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player player, ItemStack itemStack) {
                        showPage("landing.mounts", player);
                    }
                };

                MenuButton button = new MenuButton(14, stack, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }

        // Morphs
        {
            ItemStack stack = new ItemStack(Material.LEATHER, 1);

            {
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName("Morphs");
                stack.setItemMeta(meta);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player player, ItemStack itemStack) {
                        showPage("landing.morphs", player);
                    }
                };

                MenuButton button = new MenuButton(16, stack, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }

        // Gadgets
        {
            ItemStack stack = new ItemStack(Material.TNT, 1);

            {
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName("Gadgets");
                stack.setItemMeta(meta);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player player, ItemStack itemStack) {
                        showPage("landing.gadgets", player);
                    }
                };

                MenuButton button = new MenuButton(22, stack, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }
    }

    public void createParticlesPage() {
        final HubPlayer hubPlayer = this.getPlayer();
        MenuPage page = new MenuPage(this, 54, "Particle Effects", hubPlayer);
        HashMap<String, Gadget> gadgets = DataCenter.retrieveData("hub", "gadgetManager", GadgetManager.class)
                .getGadgets();

        for (final Gadget gadget : gadgets.values()) {
            if (gadget.getType() == GadgetType.PARTICLE_EFFECT) {
                ItemStack icon = gadget.getIcon();

                {
                    ItemMeta meta = icon.getItemMeta();
                    meta.setDisplayName(gadget.getName());
                    icon.setItemMeta(meta);

                    ItemUtils.addGlow(icon);
                }

                {
                    MenuButtonEvent event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player player, ItemStack itemStack) {
                            if (hubPlayer.getActiveGadget(gadget.getType()) == gadget) {
                                hubPlayer.deactivateGadget(gadget.getType());
                            } else {
                                hubPlayer.activateGadget(gadget);
                            }
                        }
                    };

                    MenuButton button = new MenuButton(page.getNextSlot(true), icon, this);
                    button.setEvent(event);
                    button.join(page);
                }
            }
        }

        ((MenuButton) this.data.get("returnButton")).join(page);

        this.putPage("landing.particles", page);
    }

    public void createPetsPage() {
        final HubPlayer hubPlayer = this.getPlayer();
        MenuPage page = new MenuPage(this, 54, "Pets", hubPlayer);
        HashMap<String, Gadget> gadgets = DataCenter.retrieveData("hub", "gadgetManager", GadgetManager.class)
                .getGadgets();

        for (final Gadget gadget : gadgets.values()) {
            if (gadget.getType() == GadgetType.PET) {
                ItemStack icon = gadget.getIcon();

                {
                    ItemMeta meta = icon.getItemMeta();
                    meta.setDisplayName(gadget.getName());
                    icon.setItemMeta(meta);
                }

                {
                    MenuButtonEvent event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player player, ItemStack itemStack) {
                            if (hubPlayer.getActiveGadget(gadget.getType()) == gadget) {
                                hubPlayer.deactivateGadget(gadget.getType());
                            } else {
                                hubPlayer.activateGadget(gadget);
                            }
                        }
                    };

                    MenuButton button = new MenuButton(page.getNextSlot(true), icon, this);
                    button.setEvent(event);
                    button.join(page);
                }
            }
        }

        ((MenuButton) this.data.get("returnButton")).join(page);

        this.putPage("landing.pets", page);
    }

    public void createMountsPage() {
        final HubPlayer hubPlayer = this.getPlayer();
        MenuPage page = new MenuPage(this, 54, "Mounts", hubPlayer);
        HashMap<String, Gadget> gadgets = DataCenter.retrieveData("hub", "gadgetManager", GadgetManager.class)
                .getGadgets();

        for (final Gadget gadget : gadgets.values()) {
            if (gadget.getType() == GadgetType.MOUNT) {
                ItemStack icon = gadget.getIcon();

                {
                    ItemMeta meta = icon.getItemMeta();
                    meta.setDisplayName(gadget.getName());
                    icon.setItemMeta(meta);
                }

                {
                    MenuButtonEvent event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player player, ItemStack itemStack) {
                            if (hubPlayer.getActiveGadget(gadget.getType()) == gadget) {
                                hubPlayer.deactivateGadget(gadget.getType());
                            } else {
                                hubPlayer.activateGadget(gadget);
                            }
                        }
                    };

                    MenuButton button = new MenuButton(page.getNextSlot(true), icon, this);
                    button.setEvent(event);
                    button.join(page);
                }
            }
        }

        ((MenuButton) this.data.get("returnButton")).join(page);

        this.putPage("landing.mounts", page);
    }

    public void createMorphsPage() {
        final HubPlayer hubPlayer = this.getPlayer();
        MenuPage page = new MenuPage(this, 54, "Morphs", hubPlayer);
        HashMap<String, Gadget> gadgets = DataCenter.retrieveData("hub", "gadgetManager", GadgetManager.class)
                .getGadgets();

        for (final Gadget gadget : gadgets.values()) {
            if (gadget.getType() == GadgetType.MORPH) {
                ItemStack icon = gadget.getIcon();

                {
                    ItemMeta meta = icon.getItemMeta();
                    meta.setDisplayName(gadget.getName());
                    icon.setItemMeta(meta);
                }

                {
                    MenuButtonEvent event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player player, ItemStack itemStack) {
                            if (hubPlayer.getActiveGadget(gadget.getType()) == gadget) {
                                hubPlayer.deactivateGadget(gadget.getType());
                            } else {
                                hubPlayer.activateGadget(gadget);
                            }
                        }
                    };

                    MenuButton button = new MenuButton(page.getNextSlot(true), icon, this);
                    button.setEvent(event);
                    button.join(page);
                }
            }
        }

        ((MenuButton) this.data.get("returnButton")).join(page);

        this.putPage("landing.morphs", page);
    }

    public void createGadgetsPage() {
        final HubPlayer hubPlayer = this.getPlayer();
        MenuPage page = new MenuPage(this, 54, "Gadgets", hubPlayer);
        HashMap<String, Gadget> gadgets = DataCenter.retrieveData("hub", "gadgetManager", GadgetManager.class)
                .getGadgets();

        for (final Gadget gadget : gadgets.values()) {
            if (gadget.getType() == GadgetType.GADGET) {
                ItemStack icon = gadget.getIcon();

                {
                    ItemMeta meta = icon.getItemMeta();
                    meta.setDisplayName(gadget.getName());
                    icon.setItemMeta(meta);
                }

                {
                    MenuButtonEvent event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player player, ItemStack itemStack) {
                            if (hubPlayer.getActiveGadget(gadget.getType()) == gadget) {
                                hubPlayer.deactivateGadget(gadget.getType());
                            } else {
                                hubPlayer.activateGadget(gadget);
                            }
                        }
                    };

                    MenuButton button = new MenuButton(page.getNextSlot(true), icon, this);
                    button.setEvent(event);
                    button.join(page);
                }
            }
        }

        ((MenuButton) this.data.get("returnButton")).join(page);

        this.putPage("landing.gadgets", page);
    }

    public void createReturnButton() {
        ItemStack icon = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

        {
            SkullMeta meta = (SkullMeta) icon.getItemMeta();
            meta.setDisplayName("Return");
            meta.setOwner("MHF_ArrowLeft");
            icon.setItemMeta(meta);
        }

        {
            MenuButtonEvent event = new MenuButtonEvent() {
                @Override
                public void execute(Player player, ItemStack itemStack) {
                    showPage("landing", player);
                }
            };

            MenuButton button = new MenuButton(40, icon, this);
            button.setEvent(event);

            this.data.put("returnButton", button);
        }
    }

    public void createDAGadgetsButton() {
        final HubPlayer hubPlayer = this.getPlayer();
        ItemStack icon = new ItemStack(Material.REDSTONE_BLOCK, 1);

        {
            ItemMeta meta = icon.getItemMeta();
            meta.setDisplayName("Deactivate all Gadgets");
            icon.setItemMeta(meta);
        }

        {
            MenuButtonEvent event = new MenuButtonEvent() {
                @Override
                public void execute(Player player, ItemStack itemStack) {
                    hubPlayer.deactivateGadgets();
                }
            };

            MenuButton button = new MenuButton(40, icon, this);
            button.setEvent(event);
            button.join(this.getPage("landing"));
        }
    }

    public void create() {
        this.createReturnButton();
        this.createDAGadgetsButton();

        this.createParticlesPage();
        this.createPetsPage();
        this.createMountsPage();
        this.createMorphsPage();
        this.createGadgetsPage();
        this.createCategories();
    }

    /** Getters **/
    public HubPlayer getPlayer() {
        return (HubPlayer) this.data.get("player");
    }
}

package com.trixo.hub.menus;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.trixo.constants.ChatConstants.ChatColours;
import com.trixo.constants.ChatConstants.ChatPrefixes;
import com.trixo.data.DataContainer;
import com.trixo.hub.player.HubPlayer;
import com.trixo.server.chat.ChatFormatter;
import com.trixo.server.inventory.Menu;
import com.trixo.server.inventory.MenuButton;
import com.trixo.server.inventory.MenuButtonEvent;
import com.trixo.server.inventory.MenuPage;

public class PlayerMenu extends Menu {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public PlayerMenu(int size, String name, DataContainer<String, Object> data) {
        super();

        this.data = new DataContainer<String, Object>(false);
        this.data.put("player", data.get("player"));
        this.data.put("targetPlayer", data.get("targetPlayer"));
        this.putPage("landing", new MenuPage(this, size, name, this.getPlayer()));
    }

    /** PlayerMenu Functions **/
    private void createFriendButton() {
        final HubPlayer targetPlayer = this.getTargetPlayer();
        final HubPlayer player = this.getPlayer();

        {
            ItemStack icon = new ItemStack(Material.RED_ROSE, 1);

            {
                ItemMeta meta = icon.getItemMeta();
                meta.setDisplayName("Friends");
                icon.setItemMeta(meta);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player p, ItemStack stack) {
                        getPlayer().removeFriend(targetPlayer.getPlayerInstance().getUniqueId());

                        targetPlayer.getPlayerInstance().sendMessage(ChatFormatter
                                .create(ChatColours.SYSTEM_DEFAULT.getColour(), ChatPrefixes.FRIENDS.toString(),
                                        player.getPlayerInstance().getName() + " removed you from their friends list!")
                                .toString());

                        player.getPlayerInstance()
                                .sendMessage(ChatFormatter.create(ChatColours.SYSTEM_DEFAULT
                                        .getColour(), ChatPrefixes.FRIENDS.toString(),
                                "removed " + targetPlayer.getPlayerInstance().getName() + " from your friends list!")
                                .toString());
                        
                        showPage("landing", p);
                        create();
                    }
                };

                MenuButton button = new MenuButton(10, icon, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }
    }

    private void createFriendPendingButton() {
        final HubPlayer targetPlayer = this.getTargetPlayer();
        final HubPlayer player = this.getPlayer();

        {
            ItemStack icon = new ItemStack(Material.PAPER, 1);
            MenuButtonEvent event = null;
            
            if ((boolean) player.getFriendStatus(targetPlayer.getPlayerInstance().getUniqueId()).get("inviter")) {
                ItemMeta meta = icon.getItemMeta();
                meta.setDisplayName(
                        targetPlayer.getPlayerInstance().getName() + " has yet to respond to your request.");
                icon.setItemMeta(meta);
                
                {
                    event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player p, ItemStack stack) {
                            
                        }
                    };
                }
            } else {
                ItemMeta meta = icon.getItemMeta();
                meta.setDisplayName(
                        "Click to accept " + targetPlayer.getPlayerInstance().getName() + "'s friend request.");
                icon.setItemMeta(meta);
                
                {
                    event = new MenuButtonEvent() {
                        @Override
                        public void execute(Player p, ItemStack stack) {
                            player.acceptFriend(targetPlayer.getPlayerInstance().getUniqueId());

                            if (player.isFriend(targetPlayer.getPlayerInstance().getUniqueId())) {
                                player.getPlayerInstance().sendMessage(ChatFormatter
                                        .create(ChatColours.SYSTEM_DEFAULT.getColour(), ChatPrefixes.FRIENDS.toString(),
                                                ChatColours.SYSTEM_DEFAULT.getColour() + "added "
                                                        + targetPlayer.getPlayerInstance().getName() + " to your friends list!")
                                        .toString());

                                targetPlayer.getPlayerInstance()
                                        .sendMessage(ChatFormatter
                                                .create(ChatColours.SYSTEM_DEFAULT.getColour(), ChatPrefixes.FRIENDS.toString(),
                                                        player.getPlayerInstance().getName() + " accepted your friend request!")
                                                .toString());
                            }
                            
                            showPage("landing", p);
                            create();
                        }
                    };
                }
            }

            MenuButton button = new MenuButton(10, icon, this);
            button.setEvent(event);
            button.join(this.getLandingPage());
        }
    }

    private void createFriendInviteButton() {
        final HubPlayer targetPlayer = this.getTargetPlayer();
        final HubPlayer player = this.getPlayer();

        {
            ItemStack icon = new ItemStack(Material.DEAD_BUSH, 1);

            {
                ItemMeta meta = icon.getItemMeta();
                meta.setDisplayName("Not friends");
                icon.setItemMeta(meta);
            }

            {
                MenuButtonEvent event = new MenuButtonEvent() {
                    @Override
                    public void execute(Player p, ItemStack stack) {
                        player.addFriend(targetPlayer.getPlayerInstance().getUniqueId());

                        player.getPlayerInstance()
                                .sendMessage(
                                        ChatFormatter
                                                .create(ChatColours.SYSTEM_DEFAULT.getColour(),
                                                        ChatPrefixes.FRIENDS.toString(),
                                                        "sent a friend request to "
                                                                + targetPlayer.getPlayerInstance().getName() + "!")
                                        .toString());

                        targetPlayer.getPlayerInstance()
                                .sendMessage(ChatFormatter
                                        .create(ChatColours.SYSTEM_DEFAULT.getColour(), ChatPrefixes.FRIENDS.toString(),
                                                player.getPlayerInstance().getName() + " sent a friend request!")
                                        .toString());

                        showPage("landing", p);
                        create();
                    }
                };

                MenuButton button = new MenuButton(10, icon, this);
                button.setEvent(event);
                button.join(this.getLandingPage());
            }
        }
    }

    /*
     * private void createFriendButton(final Menu tmp) { final TrixoPlayer
     * player = this.getPlayer(); final TrixoPlayer targetPlayer =
     * this.getTargetPlayer();
     * 
     * { ItemStack stack = null; MenuButtonEvent event = null;
     * 
     * if (player.isFriend(targetPlayer.getPlayerInstance().getUniqueId())) {
     * stack = new ItemStack(Material.RED_ROSE, 1); ItemMeta meta =
     * stack.getItemMeta(); meta.setDisplayName("Friends");
     * stack.setItemMeta(meta);
     * 
     * event = new MenuButtonEvent() {
     * 
     * @Override public void execute(Player p, ItemStack s) {
     * player.removeFriend(targetPlayer.getPlayerInstance().getUniqueId());
     * 
     * targetPlayer.getPlayerInstance().sendMessage(ChatFormatter
     * .create(ChatColours.SYSTEM_DEFAULT.getColour(),
     * ChatPrefixes.FRIENDS.toString(), player.getPlayerInstance().getName() +
     * " removed you from their friends list!") .toString());
     * 
     * player.getPlayerInstance()
     * .sendMessage(ChatFormatter.create(ChatColours.SYSTEM_DEFAULT
     * .getColour(), ChatPrefixes.FRIENDS.toString(), "removed " +
     * targetPlayer.getPlayerInstance().getName() + " from your friends list!")
     * .toString());
     * 
     * tmp.removeButton(10); createFriendButton(tmp); } }; } else if (((int)
     * player.getFriendStatus(targetPlayer.getPlayerInstance().getUniqueId())
     * .get("friendStatus")) == 1) { stack = new ItemStack(Material.PAPER, 1);
     * ItemMeta meta = stack.getItemMeta();
     * 
     * if ((boolean)
     * player.getFriendStatus(targetPlayer.getPlayerInstance().getUniqueId()).
     * get("inviter")) { meta.setDisplayName(
     * targetPlayer.getPlayerInstance().getName() +
     * " has yet to respond to your request.");
     * 
     * event = new MenuButtonEvent(); } else { meta.setDisplayName(
     * "Click to accept " + targetPlayer.getPlayerInstance().getName() +
     * "'s friend request.");
     * 
     * event = new MenuButtonEvent() {
     * 
     * @Override public void execute(Player p, ItemStack s) {
     * player.acceptFriend(targetPlayer.getPlayerInstance().getUniqueId());
     * 
     * if (player.isFriend(targetPlayer.getPlayerInstance().getUniqueId())) {
     * player.getPlayerInstance().sendMessage(ChatFormatter.create(
     * ChatColours.SYSTEM_DEFAULT.getColour(), ChatPrefixes.FRIENDS.toString(),
     * ChatColours.SYSTEM_DEFAULT.getColour() + "added " +
     * targetPlayer.getPlayerInstance().getName() + " to your friends list!")
     * .toString());
     * 
     * targetPlayer.getPlayerInstance().sendMessage(ChatFormatter
     * .create(ChatColours.SYSTEM_DEFAULT.getColour(),
     * ChatPrefixes.FRIENDS.toString(), player.getPlayerInstance().getName() +
     * " accepted your friend request!") .toString()); }
     * 
     * tmp.removeButton(10); createFriendButton(tmp); } }; }
     * 
     * stack.setItemMeta(meta); } else if
     * (!player.isFriend(targetPlayer.getPlayerInstance().getUniqueId())) {
     * stack = new ItemStack(Material.DEAD_BUSH, 1); ItemMeta meta =
     * stack.getItemMeta(); meta.setDisplayName("Not friends!");
     * stack.setItemMeta(meta);
     * 
     * event = new MenuButtonEvent() {
     * 
     * @Override public void execute(Player p, ItemStack s) {
     * player.addFriend(targetPlayer.getPlayerInstance().getUniqueId());
     * 
     * player.getPlayerInstance() .sendMessage( ChatFormatter
     * .create(ChatColours.SYSTEM_DEFAULT.getColour(),
     * ChatPrefixes.FRIENDS.toString(), "sent a friend request to " +
     * targetPlayer.getPlayerInstance().getName() + "!") .toString());
     * 
     * targetPlayer.getPlayerInstance() .sendMessage(ChatFormatter
     * .create(ChatColours.SYSTEM_DEFAULT.getColour(),
     * ChatPrefixes.FRIENDS.toString(), player.getPlayerInstance().getName() +
     * " sent a friend request!") .toString());
     * 
     * tmp.removeButton(10); createFriendButton(tmp); } }; }
     * 
     * MenuButton friendButton = new MenuButton(10, stack, tmp);
     * friendButton.setEvent(event); friendButton.join(); } }
     */

    public void create() {
        /* Friend Button */
        {
            this.createFriendButton();

            if (this.getPlayer().isFriend(this.getTargetPlayer().getPlayerInstance().getUniqueId())) {
                this.createFriendButton();
            } else if (((int) this.getPlayer().getFriendStatus(this.getTargetPlayer().getPlayerInstance().getUniqueId())
                    .get("friendStatus")) == 1) {
                this.createFriendPendingButton();
            } else if (!this.getPlayer().isFriend(this.getTargetPlayer().getPlayerInstance().getUniqueId())) {
                this.createFriendInviteButton();
            }
        }
    }

    /** Getters **/
    public HubPlayer getPlayer() {
        return (HubPlayer) this.data.get("player");
    }

    public HubPlayer getTargetPlayer() {
        return (HubPlayer) this.data.get("targetPlayer");
    }
}

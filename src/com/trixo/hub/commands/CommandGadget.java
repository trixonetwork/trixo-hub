package com.trixo.hub.commands;

import java.util.ArrayList;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.hub.menus.GadgetMenu;
import com.trixo.hub.player.HubPlayer;
import com.trixo.hub.player.HubPlayerManager;
import com.trixo.server.command.TrixoCommand;

public class CommandGadget extends TrixoCommand {
    public CommandGadget(String command) {
        super(command);
    }

    @Override
    public boolean executeCommand(CommandSender sender, ArrayList<String> arguments) {
        if (sender instanceof Player) {
            HubPlayer player = DataCenter.retrieveData("hub", "hubPlayerManager", HubPlayerManager.class)
                    .getPlayer(((Player) sender).getUniqueId());
            
            if (player != null) {
                DataContainer<String, Object> data = new DataContainer<String, Object>(false);
                data.put("player", player);

                GadgetMenu menu = new GadgetMenu(54, "Gadgets", data);
                menu.create();

                player.openMenu(menu);
            }
        }

        return false;
    }
}

package com.trixo.hub.permissions.nodes;

import com.trixo.server.permissions.TrixoPermission;

public enum TrixoPermissions {
    /** Constants **/
    GAMEMODE(new TrixoPermission("trixo.gamemode")),
    BUILD(new TrixoPermission("trixo.build")),
    BAN(new TrixoPermission("trixo.ban")),
    UNBAN(new TrixoPermission("trixo.unban")),
    KICK(new TrixoPermission("trixo.kick")),
    WARN(new TrixoPermission("trixo.warn")),
    MUTE(new TrixoPermission("trixo.mute")),
    UNMUTE(new TrixoPermission("trixo.unmute")),
    ANTISWEAR(new TrixoPermission("trixo.antiswear"));

    /** Enum **/
    private TrixoPermission permissionNode = null;

    /* Constructor */
    TrixoPermissions(TrixoPermission permissionNode) {
        this.permissionNode = permissionNode;
    }

    /* Getters */
    public TrixoPermission getPermissionNode() {
        return this.permissionNode;
    }
}

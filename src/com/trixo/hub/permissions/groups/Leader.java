package com.trixo.hub.permissions.groups;

import org.bukkit.ChatColor;

import com.trixo.server.permissions.PermissionGroup;

public class Leader extends PermissionGroup {
    public Leader() {
        this.setTagColour(ChatColor.DARK_RED);
        this.setTag("LEADER");
    }

    @Override
    public boolean hasAllPermissions() {
        return true;
    }
}

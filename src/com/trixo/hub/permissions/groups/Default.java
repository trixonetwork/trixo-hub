package com.trixo.hub.permissions.groups;

import org.bukkit.ChatColor;

import com.trixo.server.permissions.PermissionGroup;

public class Default extends PermissionGroup {
    public Default() {
        this.setTagColour(ChatColor.DARK_RED);
        this.setTag("");
    }

    @Override
    public boolean hasAllPermissions() {
        return false;
    }
}

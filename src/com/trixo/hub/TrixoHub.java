package com.trixo.hub;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.plugin.java.JavaPlugin;

import com.trixo.data.DataCenter;
import com.trixo.hub.commands.CommandGadget;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetManager;
import com.trixo.hub.cosmetic.gadget.morph.SlimeMorphGadget;
import com.trixo.hub.cosmetic.gadget.mount.HorseMountGadget;
import com.trixo.hub.cosmetic.gadget.mount.SheepMountGadget;
import com.trixo.hub.cosmetic.gadget.mount.SpiderMountGadget;
import com.trixo.hub.cosmetic.gadget.other.BowGadget;
import com.trixo.hub.cosmetic.gadget.particle.TestParticle;
import com.trixo.hub.cosmetic.gadget.pet.CatPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.ChickenPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.CowPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.DogPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.HorsePetGadget;
import com.trixo.hub.cosmetic.gadget.pet.MooshroomPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.PigPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.RabbitPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.SheepPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.VillagerPetGadget;
import com.trixo.hub.cosmetic.gadget.pet.ZombiePetGadget;
import com.trixo.hub.cosmetic.mount.MountManager;
import com.trixo.hub.cosmetic.pet.PetManager;
import com.trixo.hub.permissions.groups.Default;
import com.trixo.hub.permissions.groups.Leader;
import com.trixo.hub.player.HubPlayerManager;
import com.trixo.hub.player.PlayerEvents;
import com.trixo.io.TrixoClassLoader;
import com.trixo.server.permissions.GroupManager;

public class TrixoHub extends JavaPlugin {
    private PlayerEvents events = new PlayerEvents();
    private CommandGadget command = new CommandGadget("gadget");

    /** JavaPlugin Functions **/
    @Override
    public void onEnable() {
        this.initPermissions();

        this.command.register();
        this.events.register(this);

        /** Adding Default Data **/
        /* Global Data */
        {
            PetManager petManager = new PetManager();
            petManager.register(this);

            DataCenter.putData("hub", "petManager", petManager);
        }
        
        {
            MountManager mountManager = new MountManager();
            mountManager.register(this);

            DataCenter.putData("hub", "mountManager", mountManager);
        }

        {
            {
                HubPlayerManager playerManager = new HubPlayerManager(this);
                playerManager.loadPlayers();
                playerManager.register(this);

                DataCenter.putData("hub", "hubPlayerManager", playerManager);
            }

            DataCenter.putData("hub", "plugin", this);
        }

        /* Gadgets */
        {
            GadgetManager manager = new GadgetManager();

            // Classloader Gadgets
            {
                ArrayList<Class<?>> classes = new TrixoClassLoader().loadClasses(new File("trixo/classes/gadgets/"));

                for (Class<?> c : classes) {
                    try {
                        manager.putGadget((Gadget) c.newInstance());
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

            // Pet Gadgets
            {
                manager.putGadget(new CatPetGadget());
                manager.putGadget(new DogPetGadget());
                manager.putGadget(new VillagerPetGadget());
                manager.putGadget(new CowPetGadget());
                manager.putGadget(new MooshroomPetGadget());
                manager.putGadget(new PigPetGadget());
                manager.putGadget(new SheepPetGadget());
                manager.putGadget(new ZombiePetGadget());
                manager.putGadget(new HorsePetGadget());
                manager.putGadget(new ChickenPetGadget());
                manager.putGadget(new RabbitPetGadget());
            }
            
            // Mount Gadgets
            {
                manager.putGadget(new HorseMountGadget());
                manager.putGadget(new SpiderMountGadget());
                manager.putGadget(new SheepMountGadget());
            }
            
            // Morph Gadgets
            {
                manager.putGadget(new SlimeMorphGadget());
            }
            
            // Particle Gadgets
            {
                manager.putGadget(new TestParticle());
            }
            
            // Other Gadgets
            {
                manager.putGadget(new BowGadget());
            }

            DataCenter.putData("hub", "gadgetManager", manager);
        }
    }

    @Override
    public void onDisable() {
        HubPlayerManager playerManager = DataCenter.retrieveData("hub", "hubPlayerManager", HubPlayerManager.class);
        DataCenter.retrieveData("hub", "petManager", PetManager.class).unregister();

        this.events.unregister();
        playerManager.unregister();
        playerManager.removePlayers();
        playerManager = null;
    }

    /** Start Functions **/
    public void initPermissions() {
        GroupManager.addPermissionGroup("DEFAULT", new Default());
        GroupManager.addPermissionGroup("LEADER", new Leader());
    }
}
package com.trixo.hub.cosmetic.pet;

import org.bukkit.entity.EntityType;

public enum PetType {
    /** Enum Data **/
    VILLAGER(EntityType.VILLAGER, "Villager", true, true, false),
    OCELOT(EntityType.OCELOT, "Cat", true, true, true),
    WOLF(EntityType.WOLF, "Dog", true, true, true),
    COW(EntityType.COW, "Cow", true, true, false),
    MOOSHROOM(EntityType.MUSHROOM_COW, "Mooshroom", true, true, false),
    PIG(EntityType.PIG, "Pig", true, true, false),
    SHEEP(EntityType.SHEEP, "Sheep", true, true, false),
    HORSE(EntityType.HORSE, "Horse", true, true, true),
    ZOMBIE(EntityType.ZOMBIE, "Zombie", true, false, false),
    CHICKEN(EntityType.CHICKEN, "Chicken", true, true, false),
    RABBIT(EntityType.RABBIT, "Rabbit", true, true, false);
    
    /** Enum Structure **/
    private EntityType type = null;
    private String name = null;
    private boolean isBaby = false;
    private boolean isAgeable = false;
    private boolean isTameable = false;

    /* Constructor */
    PetType(EntityType type, String name, boolean isBaby, boolean isAgeable, boolean isTameable) {
        this.type = type;
        this.name = name;
        this.isBaby = isBaby;
        this.isAgeable = isAgeable;
        this.isTameable = isTameable;
    }

    /* Getters */
    public EntityType getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public boolean getIsBaby() {
        return this.isBaby;
    }

    public boolean getIsAgeable() {
        return this.isAgeable;
    }

    public boolean getIsTameable() {
        return this.isTameable;
    }
}

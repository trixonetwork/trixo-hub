package com.trixo.hub.cosmetic.pet;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import com.trixo.data.DataContainer;
import com.trixo.hub.player.HubPlayer;

public class Pet {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public Pet() {
        this.data = new DataContainer<String, Object>(false);
    }

    /** Getters, Setters **/
    /* Getters */
    public String getName() {
        return (String) this.data.get("name");
    }
    
    public boolean getIsBaby() {
        return (boolean) this.data.get("isBaby");
    }
    
    public EntityType getEntityType() {
        return (EntityType) this.data.get("type");
    }
    
    public Entity getEntity() {
        return (Entity) this.data.get("entity");
    }
    
    public HubPlayer getOwner() {
        return (HubPlayer) this.data.get("owner");
    }

    /* Setters */
    public void setName(String name) {
        this.data.put("name", name);
    }
    
    public void setIsBaby(boolean isBaby) {
        this.data.put("isBaby", isBaby);
    }
    
    public void setEntityType(EntityType type) {
        this.data.put("type", type);
    }
    
    public void setEntity(Entity entity) {
        this.data.put("entity", entity);
    }
    
    public void setOwner(HubPlayer owner) {
        this.data.put("owner", owner);
    }
}

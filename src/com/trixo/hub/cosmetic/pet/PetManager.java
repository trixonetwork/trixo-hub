package com.trixo.hub.cosmetic.pet;

import java.util.UUID;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataContainer;
import com.trixo.hub.player.HubPlayer;
import com.trixo.server.event.ITrixoEvent;
import com.trixo.utils.EntityUtils;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;

@SuppressWarnings("unchecked")
public class PetManager implements ITrixoEvent {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public PetManager() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("activePets", new DataContainer<UUID, Pet>(false));
    }

    /** PetManager Functions **/
    public void spawnPet(HubPlayer player, PetType type) {
        this.removePet(player);

        {
            Pet pet = new Pet();
            pet.setOwner(player);
            pet.setEntityType(type.getType());
            pet.setName(type.getName());
            pet.setIsBaby(type.getIsBaby());
            
            {
                Entity entity = player.getWorld().spawnEntity(player.getLocation(), type.getType());
                entity.setCustomName(pet.getName());
                
                EntityUtils.clearAI(entity);
                EntityUtils.setAIGoal(entity, 0, new PathfinderGoalLookAtPlayer(
                        (EntityInsentient) ((CraftLivingEntity) entity).getHandle(), EntityHuman.class, 12.0F, 1.0F));

                {
                    if (type.getIsTameable()) {
                        ((Tameable) entity).setOwner(player.getPlayerInstance());
                    }

                    if (type.getIsAgeable() && pet.getIsBaby()) {
                        ((Ageable) entity).setBaby();
                    }

                    if (type == PetType.ZOMBIE) {
                        ((Zombie) entity).setBaby(pet.getIsBaby());
                    }
                }

                pet.setEntity(entity);
            }

            this.addActivePet(player, pet);
        }
    }

    public void removePet(HubPlayer player) {
        if (this.getActivePets().containsKey(player.getUUID())) {
            this.getActivePet(player).getEntity().remove();
        }
    }

    /** Getters, Adders **/
    /* Getters */
    public DataContainer<UUID, Pet> getActivePets() {
        return (DataContainer<UUID, Pet>) this.data.get("activePets");
    }

    public Pet getActivePet(HubPlayer player) {
        return this.getActivePets().get(player.getUUID());
    }

    /* Adders */
    public void addActivePet(HubPlayer player, Pet pet) {
        this.getActivePets().put(player.getUUID(), pet);
    }

    /** Events **/
    /* Register */
    @Override
    public void register(Plugin plugin) {
        this.data.put("registered", true);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        this.data.put("registered", false);

        HandlerList.unregisterAll(this);
    }

    /* Events */
    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        for (Pet pet : this.getActivePets().valueSet()) {
            if (pet.getEntity() == event.getEntity())
                event.setCancelled(true);
        }
    }
}

package com.trixo.hub.cosmetic.gadget.pet;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.pet.PetType;
import com.trixo.hub.player.HubPlayer;

@SuppressWarnings("deprecation")
public class ZombiePetGadget extends PetGadget {
    /** Constructor **/
    public ZombiePetGadget() {
        super("pets.ZombiePet", "Zombie Pet");
    }
    
    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getPetManager().spawnPet(player, PetType.ZOMBIE);
    }
    
    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.MONSTER_EGG, 1, EntityType.ZOMBIE.getTypeId());
    }
}

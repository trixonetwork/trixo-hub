package com.trixo.hub.cosmetic.gadget.pet;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.pet.PetType;
import com.trixo.hub.player.HubPlayer;

@SuppressWarnings("deprecation")
public class RabbitPetGadget extends PetGadget {
    /** Constructor **/
    public RabbitPetGadget() {
        super("pets.RabbitPet", "Rabbit Pet");
    }
    
    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getPetManager().spawnPet(player, PetType.RABBIT);
    }
    
    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.MONSTER_EGG, 1, EntityType.RABBIT.getTypeId());
    }
}

package com.trixo.hub.cosmetic.gadget.pet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftCreature;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.trixo.data.DataCenter;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.cosmetic.pet.Pet;
import com.trixo.hub.cosmetic.pet.PetManager;
import com.trixo.hub.player.HubPlayer;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.Navigation;

public class PetGadget extends Gadget {
    private PetManager petManager = null;
    private int failedAttempts = 0;

    /** Constructors **/
    public PetGadget(String gadgetID, String name) {
        super(gadgetID, name, GadgetType.PET, 0);

        this.petManager = DataCenter.retrieveData("hub", "petManager", PetManager.class);
    }

    /** Gadget Functions **/
    @Override
    public void onDeactivated(HubPlayer player) {
        this.petManager.removePet(player);
    }

    @Override
    public void onTick(HubPlayer player) {
        Pet pet = this.petManager.getActivePet(player);

        Location petSpot = pet.getEntity().getLocation();
        Location ownerSpot = player.getLocation();
        int xDiff = Math.abs(petSpot.getBlockX() - ownerSpot.getBlockX());
        int yDiff = Math.abs(petSpot.getBlockY() - ownerSpot.getBlockY());
        int zDiff = Math.abs(petSpot.getBlockZ() - ownerSpot.getBlockZ());

        if ((xDiff + yDiff + zDiff) > 4) {
            EntityCreature ec = ((CraftCreature) pet.getEntity()).getHandle();
            Navigation nav = (Navigation) ec.getNavigation();

            int xIndex = -1;
            int zIndex = -1;

            Block targetBlock = ownerSpot.getBlock().getRelative(xIndex, -1, zIndex);
            while (targetBlock.isEmpty() || targetBlock.isLiquid()) {
                if (xIndex < 2)
                    xIndex++;
                else if (zIndex < 2) {
                    xIndex = -1;
                    zIndex++;
                } else
                    return;

                targetBlock = ownerSpot.getBlock().getRelative(xIndex, -1, zIndex);
            }

            float speed = 0.9f;
            if (pet.getEntityType() == EntityType.VILLAGER)
                speed = 0.6f;

            if (this.failedAttempts > 4) {
                pet.getEntity().teleport(player.getPlayerInstance());
                
                this.failedAttempts = 0;
            } else if (pet.getEntity().getLocation().getY() < 0) {
                this.failedAttempts++;
            } else if (!nav.a(targetBlock.getX(), targetBlock.getY() + 1, targetBlock.getZ(), speed)) {
                if (pet.getEntity().getFallDistance() == 0) {
                    this.failedAttempts++;
                }
            } else {
                this.failedAttempts = 0;
            }
        }
    }

    @Override
    public boolean doTick() {
        return true;
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.MONSTER_EGG, 1);
    }

    protected PetManager getPetManager() {
        return this.petManager;
    }
}
package com.trixo.hub.cosmetic.gadget.pet;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.pet.PetType;
import com.trixo.hub.player.HubPlayer;

@SuppressWarnings("deprecation")
public class SheepPetGadget extends PetGadget {
    /** Constructor **/
    public SheepPetGadget() {
        super("pets.SheepPet", "Sheep Pet");
    }
    
    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getPetManager().spawnPet(player, PetType.SHEEP);
    }
    
    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.MONSTER_EGG, 1, EntityType.SHEEP.getTypeId());
    }
}

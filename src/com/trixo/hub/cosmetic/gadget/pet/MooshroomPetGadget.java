package com.trixo.hub.cosmetic.gadget.pet;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.pet.PetType;
import com.trixo.hub.player.HubPlayer;

@SuppressWarnings("deprecation")
public class MooshroomPetGadget extends PetGadget {
    /** Constructor **/
    public MooshroomPetGadget() {
        super("pets.MooshroomPet", "Mooshroom Pet");
    }
    
    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getPetManager().spawnPet(player, PetType.MOOSHROOM);
    }   
    
    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.MONSTER_EGG, 1, EntityType.MUSHROOM_COW.getTypeId());
    }
}

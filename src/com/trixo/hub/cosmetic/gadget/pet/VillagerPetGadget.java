package com.trixo.hub.cosmetic.gadget.pet;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.pet.PetType;
import com.trixo.hub.player.HubPlayer;

@SuppressWarnings("deprecation")
public class VillagerPetGadget extends PetGadget {
    /** Constructor **/
    public VillagerPetGadget() {
        super("pets.VillagerPet", "Villager Pet");
    }
    
    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getPetManager().spawnPet(player, PetType.VILLAGER);
    }
    
    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.MONSTER_EGG, 1, EntityType.VILLAGER.getTypeId());
    }
}

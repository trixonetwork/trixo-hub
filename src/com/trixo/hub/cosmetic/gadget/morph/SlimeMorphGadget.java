package com.trixo.hub.cosmetic.gadget.morph;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.trixo.constants.PlayerConstants.MorphType;
import com.trixo.hub.player.HubPlayer;

public class SlimeMorphGadget extends MorphGadget {
    /** Constructors **/
    public SlimeMorphGadget() {
        super("morphs.SlimeMorph", "Slime Morph");
    }
    
    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        player.morph(MorphType.SLIME);
    }
    
    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.SLIME_BALL, 1);
    }
}

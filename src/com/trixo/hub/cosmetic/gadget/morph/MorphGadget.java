package com.trixo.hub.cosmetic.gadget.morph;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.cosmetic.mount.MountManager;
import com.trixo.hub.player.HubPlayer;

public class MorphGadget extends Gadget {
    private MountManager mountManager = null;

    /** Constructors **/
    public MorphGadget(String gadgetID, String name) {
        super(gadgetID, name, GadgetType.MORPH, 0);
    }

    /** Gadget Functions **/
    @Override
    public void onDeactivated(HubPlayer player) {
        player.unmorph();
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.LEATHER, 1);
    }

    protected MountManager getMountManager() {
        return this.mountManager;
    }
}
package com.trixo.hub.cosmetic.gadget.mount;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftCreature;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.trixo.hub.cosmetic.mount.MountType;
import com.trixo.hub.player.HubPlayer;

import net.minecraft.server.v1_8_R3.EntityCreature;

public class SpiderMountGadget extends MountGadget {
    /** Constructors **/
    public SpiderMountGadget() {
        super("mounts.SpiderMount", "Spider Mount");
    }

    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getMountManager().spawnMount(player, MountType.SPIDER);
    }

    @Override
    public void onTick(HubPlayer player) {
        Entity entity = this.getMountManager().getActiveMount(player).getEntity();
        EntityCreature ec = ((CraftCreature) entity).getHandle();

        if (ec.motX > 0) {
            Block targetBlock = player.getLocation().getBlock().getRelative(1, 0, 0);

            if (targetBlock.getType() != Material.AIR && targetBlock.getType().isSolid()) {
                this.getMountManager().getActiveMount(player).getEntity().setVelocity(new Vector(0, 0.5, 0));
            }
        }

        if (ec.motX < 0) {
            Block targetBlock = player.getLocation().getBlock().getRelative(-1, 0, 0);

            if (targetBlock.getType() != Material.AIR && targetBlock.getType().isSolid()) {
                this.getMountManager().getActiveMount(player).getEntity().setVelocity(new Vector(0, 0.5, 0));
            }
        }

        if (ec.motZ > 0) {
            Block targetBlock = player.getLocation().getBlock().getRelative(0, 0, 1);

            if (targetBlock.getType() != Material.AIR && targetBlock.getType().isSolid()) {
                this.getMountManager().getActiveMount(player).getEntity().setVelocity(new Vector(0, 0.5, 0));
            }
        }

        if (ec.motZ < 0) {
            Block targetBlock = player.getLocation().getBlock().getRelative(0, 0, -1);

            if (targetBlock.getType() != Material.AIR && targetBlock.getType().isSolid()) {
                this.getMountManager().getActiveMount(player).getEntity().setVelocity(new Vector(0, 0.5, 0));
            }
        }
    }

    @Override
    public boolean doTick() {
        return true;
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.SPIDER_EYE, 1);
    }
}
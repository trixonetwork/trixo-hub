package com.trixo.hub.cosmetic.gadget.mount;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.trixo.data.DataCenter;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.cosmetic.mount.MountManager;
import com.trixo.hub.player.HubPlayer;

public class MountGadget extends Gadget {
    private MountManager mountManager = null;

    /** Constructors **/
    public MountGadget(String gadgetID, String name) {
        super(gadgetID, name, GadgetType.MOUNT, 0);

        this.mountManager = DataCenter.retrieveData("hub", "mountManager", MountManager.class);
    }

    /** Gadget Functions **/
    @Override
    public void onDeactivated(HubPlayer player) {
        this.mountManager.removeMount(player);
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.SADDLE, 1);
    }

    protected MountManager getMountManager() {
        return this.mountManager;
    }
}
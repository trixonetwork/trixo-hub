package com.trixo.hub.cosmetic.gadget.mount;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.mount.MountType;
import com.trixo.hub.player.HubPlayer;

public class HorseMountGadget extends MountGadget {
    /** Constructors **/
    public HorseMountGadget() {
        super("mounts.HorseMount", "Horse Mount");
    }

    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getMountManager().spawnMount(player, MountType.HORSE);
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.HAY_BLOCK, 1);
    }
}
package com.trixo.hub.cosmetic.gadget.mount;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.trixo.hub.cosmetic.mount.MountType;
import com.trixo.hub.player.HubPlayer;

public class SheepMountGadget extends MountGadget {
    /** Constructors **/
    public SheepMountGadget() {
        super("mounts.SheepMount", "Sheep Mount");
    }

    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        this.getMountManager().spawnMount(player, MountType.SHEEP);
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.WOOL, 1);
    }
}
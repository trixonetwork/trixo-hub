package com.trixo.hub.cosmetic.gadget;

public enum GadgetType {
    /** Types **/
    GADGET("gadget"),
    PET("pet"),
    MORPH("morph"),
    ARROW_TRAIL("arrow_trail"),
    DEATH_ANIMATION("death_animation"),
    MOUNT("mount"),
    PARTICLE_EFFECT("particle_effect");     
    
    /*** GadgetType ***/
    private String unlocalizedName;
    
    /** Constructor **/
    GadgetType(String unlocalizedName) {
        this.unlocalizedName = unlocalizedName;
    }
    
    /** Object Functions **/
    @Override
    public String toString() {
        return this.unlocalizedName;
    }
}

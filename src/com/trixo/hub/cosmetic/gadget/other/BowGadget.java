package com.trixo.hub.cosmetic.gadget.other;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;

import com.trixo.data.DataCenter;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.cosmetic.mount.MountManager;
import com.trixo.hub.player.HubPlayer;

public class BowGadget extends Gadget {
    private MountManager mountManager = null;

    /** Constructors **/
    public BowGadget() {
        super("other.bow", "Bow", GadgetType.GADGET, 0);

        this.mountManager = DataCenter.retrieveData("hub", "mountManager", MountManager.class);
    }

    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        player.getPlayerInstance().getInventory().setItem(0, new ItemStack(Material.BOW, 1));
        player.getPlayerInstance().getInventory().setItem(9, new ItemStack(Material.ARROW, 1));
    }

    @Override
    public void onDeactivated(HubPlayer player) {
        player.getPlayerInstance().getInventory().clear(0);
        player.getPlayerInstance().getInventory().clear(9);
    }

    @Override
    public boolean doTick() {
        return false;
    }

    /** Events **/
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Arrow) {
            if (event.getEntity().getShooter() instanceof Player) {
                Player shooter = (Player) event.getEntity().getShooter();

                {
                    BlockIterator iterator = new BlockIterator(event.getEntity().getWorld(),
                            event.getEntity().getLocation().toVector(), event.getEntity().getVelocity().normalize(),
                            0.0D, 4);

                    Block hitBlock = null;

                    while (iterator.hasNext()) {
                        hitBlock = iterator.next();

                        if (hitBlock.getType() != Material.AIR) {
                            break;
                        }
                    }

                    if (hitBlock.getType().isSolid()) {
                        Location location = event.getEntity().getLocation();
                        location.setYaw(shooter.getLocation().getYaw());
                        location.setPitch(shooter.getLocation().getPitch());

                        shooter.teleport(location.add(0, 1.5, 0));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onArrowHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Arrow) {
            Arrow arrow = (Arrow) event.getEntity();
            arrow.remove();
        }

        ((Player) event.getEntity().getShooter()).getInventory().setItem(9, new ItemStack(Material.ARROW, 1));
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.BOW, 1);
    }

    protected MountManager getMountManager() {
        return this.mountManager;
    }
}
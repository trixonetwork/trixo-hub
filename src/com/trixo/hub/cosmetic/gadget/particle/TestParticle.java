package com.trixo.hub.cosmetic.gadget.particle;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.trixo.constants.ColourConstants.BlendMode;
import com.trixo.data.DataContainer;
import com.trixo.hub.cosmetic.gadget.Gadget;
import com.trixo.hub.cosmetic.gadget.GadgetType;
import com.trixo.hub.player.HubPlayer;
import com.trixo.interfaces.IImmutable;
import com.trixo.math.colour.ColourRGB;
import com.trixo.math.vector.Vector3;
import com.trixo.server.world.particle.ParticleType;
import com.trixo.server.world.particle.ParticleType.OrdinaryColour;

@SuppressWarnings("unchecked")
public class TestParticle extends Gadget {
    /** Constructors **/
    public TestParticle() {
        super("particles.TestParticle", "Test Effect", GadgetType.PARTICLE_EFFECT, 0);
    }

    /** Gadget Functions **/
    @Override
    public void onActivated(HubPlayer player) {
        player.getPublicData().put("gadget.TestParticle.t", 0.0);
        player.getPublicData().put("gadget.TestParticle.t2", 0.0);
        player.getPublicData().put("gadget.TestParticle.invert", false);
    }

    @Override
    public void onTick(HubPlayer player) {
        DataContainer<String, Object> blendData = new DataContainer<String, Object>(false);
        double t = 0, t2 = 0;
        boolean invert = false;

        {
            t = (double) player.getPublicData().get("gadget.TestParticle.t");
            t2 = (double) player.getPublicData().get("gadget.TestParticle.t2");
            invert = (boolean) player.getPublicData().get("gadget.TestParticle.invert");
        }

        blendData.put("ratio", t2 / 2.0);

        {
            t += Math.PI / 64;

            if (invert) {
                t2 -= Math.PI / 64;
            } else {
                t2 += Math.PI / 64;
            }
        }

        {
            for (int i = 0; i < 5; i++) {
                Vector3<IImmutable> position = (Vector3<IImmutable>) player.getPosition();
                position = position.add(Math.cos(6 * t), t2, Math.sin(6 * t));

                ColourRGB<IImmutable> colour = new ColourRGB<IImmutable>(255, 0, 0) {};
                colour = (ColourRGB<IImmutable>) colour.blendColour(new ColourRGB<IImmutable>(0, 0, 255) {},
                        BlendMode.ADDITIVE_RATIO, blendData);

                ParticleType.REDSTONE.display(player.getWorld(), new OrdinaryColour(colour), position, 65565);
            }

            for (int i = 0; i < 5; i++) {
                Vector3<IImmutable> position = (Vector3<IImmutable>) player.getPosition();
                position = position.add(Math.sin(6 * t), t2, Math.cos(6 * t));

                ColourRGB<IImmutable> colour = new ColourRGB<IImmutable>(255, 255, 0) {};
                colour = (ColourRGB<IImmutable>) colour.blendColour(new ColourRGB<IImmutable>(0, 255, 0) {},
                        BlendMode.ADDITIVE_RATIO, blendData);

                ParticleType.REDSTONE.display(player.getWorld(), new OrdinaryColour(colour), position, 65565);
            }
        }

        if (t2 >= 2 || t2 < 0) {
            invert = !invert;
        }

        {
            player.getPublicData().put("gadget.TestParticle.t", t);
            player.getPublicData().put("gadget.TestParticle.t2", t2);
            player.getPublicData().put("gadget.TestParticle.invert", invert);
        }
    }

    @Override
    public void onDeactivated(HubPlayer player) {
        player.getPublicData().remove("gadget.TestParticle.t");
        player.getPublicData().remove("gadget.TestParticle.t2");
        player.getPublicData().remove("gadget.TestParticle.invert");
    }

    @Override
    public boolean doTick() {
        return true;
    }

    /** Getters **/
    /* Getters */
    @Override
    public ItemStack getIcon() {
        return new ItemStack(Material.STONE, 1);
    }
}
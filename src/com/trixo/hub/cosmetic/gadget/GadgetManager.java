package com.trixo.hub.cosmetic.gadget;

import java.util.HashMap;
import com.trixo.data.DataContainer;

@SuppressWarnings("unchecked")
public class GadgetManager {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public GadgetManager() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("gadgets", new HashMap<String, Gadget>());
    }

    /** GadgetManager Functions **/

    /** Getters, Putters **/
    /* Getters */
    public HashMap<String, Gadget> getGadgets() {
        return (HashMap<String, Gadget>) this.data.get("gadgets");
    }

    public Gadget getGadget(String ID) {
        return this.getGadgets().get(ID);
    }

    /* Putters */
    public void putGadget(Gadget gadget) {
        if (this.getGadgets().containsKey(gadget.getID())) {
            this.getGadgets().put(gadget.getID(), gadget);
        } else {
            this.getGadgets().put(gadget.getID(), gadget);
        }
    }
}

package com.trixo.hub.cosmetic.gadget;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataContainer;
import com.trixo.hub.player.HubPlayer;
import com.trixo.interfaces.IDataObject;
import com.trixo.server.event.ITrixoEvent;

public class Gadget implements ITrixoEvent, IDataObject<String, Object> {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public Gadget(String gadgetID, String name, GadgetType type, int tickSpeed) {
        this.data = new DataContainer<String, Object>(false);

        this.data.put("gadgetID", gadgetID);
        this.data.put("name", name);
        this.data.put("type", type);
        this.data.put("tickSpeed", tickSpeed);
    }

    /** Gadget Functions **/
    /* Events */
    public void onActivated(HubPlayer player) {

    }

    public void onDeactivated(HubPlayer player) {

    }

    public void onTick(HubPlayer player) {

    }

    public boolean doTick() {
        return false;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {

    }

    /** ITrixoEvent Functions **/
    @Override
    public void register(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        HandlerList.unregisterAll(this);
    }

    /** IDataObject Functions **/
    @Override
    public DataContainer<String, Object> asDataContainer() {
        return this.data;
    }

    /** Getters **/
    /* Getters */
    public String getID() {
        return (String) this.data.get("gadgetID");
    }

    public String getName() {
        return (String) this.data.get("name");
    }

    public GadgetType getType() {
        return (GadgetType) this.data.get("type");
    }

    public int getTickSpeed() {
        return (int) this.data.get("tickSpeed");
    }

    public ItemStack getIcon() {
        return new ItemStack(Material.STONE, 1);
    }
}

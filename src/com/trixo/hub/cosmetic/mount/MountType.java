package com.trixo.hub.cosmetic.mount;

import com.trixo.constants.PlayerConstants.MorphType;

public enum MountType {
    /** Enum Data **/
    HORSE(MorphType.HORSE, "Horse"),
    SHEEP(MorphType.SHEEP, "Sheep"),
    PIG(MorphType.PIG, "Pig"),
    COW(MorphType.COW, "Cow"),
    CHICKEN(MorphType.CHICKEN, "Chicken"),
    ZOMBIE(MorphType.ZOMBIE, "Zombie"),
    SKELETON(MorphType.SKELETON, "Skeleton"),
    WITHER_SKELETON(MorphType.WITHER_SKELETON, "WitherSkeleton"),
    SPIDER(MorphType.SPIDER, "Spider"),
    CAVE_SPIDER(MorphType.CAVE_SPIDER, "CaveSpider"),
    SLIME(MorphType.SLIME, "Slime");

    /** Enum Structure **/
    private MorphType type = null;
    private String name = null;

    /* Constructor */
    MountType(MorphType type, String name) {
        this.type = type;
        this.name = name;
    }

    /* Getters */
    public MorphType getMorphType() {
        return this.type;
    }
    
    public String getName() {
        return this.name;
    }
}
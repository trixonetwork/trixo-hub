package com.trixo.hub.cosmetic.mount;

import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataContainer;
import com.trixo.hub.player.HubPlayer;
import com.trixo.server.entity.EntityManager;
import com.trixo.server.event.ITrixoEvent;
import com.trixo.utils.EntityUtils;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;

@SuppressWarnings("unchecked")
public class MountManager implements ITrixoEvent {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public MountManager() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("activeMounts", new DataContainer<UUID, Mount>(false));
    }

    /** PetManager Functions **/
    public void spawnMount(HubPlayer player, MountType type) {
        this.removeMount(player);

        {
            Mount mount = new Mount();
            mount.setName(type.getName());
            mount.setOwner(player);

            {
                Horse horse = (Horse) player.getWorld().spawnEntity(player.getLocation(), EntityType.HORSE);
                horse.setOwner(player.getPlayerInstance());
                horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
                horse.setCustomName(mount.getName());

                {
                    EntityUtils.setEntitySilent(horse, true);
                    EntityUtils.clearAI(horse);
                    EntityUtils.setAIGoal(horse, 0,
                            new PathfinderGoalLookAtPlayer((EntityInsentient) ((CraftLivingEntity) horse).getHandle(),
                                    EntityHuman.class, 12.0F, 1.0F));

                    if (type != MountType.HORSE) {
                        EntityManager.morphEntity(horse, type.getMorphType());
                    }
                }

                mount.setEntity(horse);
            }

            this.addActiveMount(player, mount);
        }
    }

    public void removeMount(HubPlayer player) {
        if (this.getActiveMounts().containsKey(player.getUUID())) {
            EntityManager.unmorphEntity(this.getActiveMount(player).getEntity());

            this.getActiveMount(player).getEntity().remove();
        }
    }

    /** Getters, Adders **/
    /* Getters */
    public DataContainer<UUID, Mount> getActiveMounts() {
        return (DataContainer<UUID, Mount>) this.data.get("activeMounts");
    }

    public Mount getActiveMount(HubPlayer player) {
        return this.getActiveMounts().get(player.getUUID());
    }

    /* Adders */
    public void addActiveMount(HubPlayer player, Mount mount) {
        this.getActiveMounts().put(player.getUUID(), mount);
    }

    /** Events **/
    /* Register */
    @Override
    public void register(Plugin plugin) {
        this.data.put("registered", true);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        this.data.put("registered", false);

        HandlerList.unregisterAll(this);
    }

    /* Events */
    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        for (Mount mount : this.getActiveMounts().valueSet()) {
            if (mount.getEntity() == event.getEntity())
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityInterect(PlayerInteractEntityEvent event) {
        for (Mount mount : this.getActiveMounts().valueSet()) {
            if (mount.getEntity() == event.getRightClicked()) {
                if (mount.getOwner().getPlayerInstance() == event.getPlayer()) {
                    mount.getEntity().setPassenger(event.getPlayer());
                } else {
                    event.getPlayer().sendMessage("u aint the owner of dis mount ho");

                    event.setCancelled(true);
                }
            }

            event.setCancelled(true);
        }
    }
}
